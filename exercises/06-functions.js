// @flow
//#### Functions ####//

// ASSIGNMENT: Change the body of this method to remove the
// type errors, but leave the type declaration unchanged.
function process(alpha: ?string, beta?: number ){
  alpha.charAt(1);
  Math.abs(beta);
}

// QUESTION: Based on what you learned when solving the
// last assignment, what is the difference between
// putting the question mark before the type and after
// the parameter?


// RETURN TYPES
//This is how you specify the return type of a method
function add(a: number, b: number): number {
  return a + b;
}

//ASSIGNMENT: Specify the return type of the next two functions
function concat(a: string, b: string) {
  return a + b;
}

function push(elements: string[], element: string) {
  return elements.push(element);
}

//Rest parameters can be typed as well
function appendAll(...list: string[]): string {
  return list.reduce((a,b) => a+b, '', list);
}
appendAll('a', 'b'); // 'ab';

// ASSIGNMENT: Analogous to appendAll, create a typechecked
// method that sums all arguments.

// function sum(...
sum(1, 2, 3.4); //6.4

// ASSIGNMENT: Optional and maybe types also work
// for object types. What are valid values for MyMaybe?
// And for MyOptional?
type MyMaybe = { first: ?boolean };
({ first: true } : MyMaybe);
//...

type MyOptional = { first?: boolean };
({ first: true } : MyOptional);
//...

// ASSIGNMENT: Make sure that you get a type error
// when you call takesTwoParams(1) with too few arguments
function takesTwoParams(a, b){ };
takesTwoParams(1);

// ASSIGNMENT: Can you explain why this trick warns
// you if you provide too many parameters?
function tooManyParams(a, ...rest:void[]){}
tooManyParams(1, 2);

// Typing functions
// Functions are just values in Javascript.
type myCallback = (a:number) => number;
function myMap( cb: myCallback, list: number[] ){
  return list.map(cb);
}
myMap( (a) => a+1, [1,2,3]); // 2,3,4
//myMap( (a) => a+'!', [1,2,3]); // This is a type error

//ASSIGNMENT: Type the following lamda:
// type dropFn = ...
const drop: dropFn = (numbers, n) => numbers.slice(n, numbers.length);
drop([1,2,3], 2); // [3]
