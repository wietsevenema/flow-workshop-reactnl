// @flow
//#### Simple types ####//

(2 : number); //No error

// ASSIGNMENT: Add specific types.
// Change all the 'any' to the most specific type.
(NaN: any);
(true: any);
((1 + "hello") : any);
(new String(1): any);
(1: any);
(3.4: any);
(null: any);
(undefined: any);
({}: any)
