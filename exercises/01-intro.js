// @flow
//#### Intro ####//

// ASSIGNMENT: Set up Atom, Nuclide, Flow and practise with
// autocompletion, fix all errors in this file.

const person = { name: 'Peter', surname: 'Pan', age: 20 };
console.log(person.naem); //This will error

function add(a, b) {
  return a + b;
}
add('1', true); //This will error, can't add a string and a boolean.
ad(1 + 1); // This will error, undefined is not a function

//Flow knows a lot about JS stdlib
Math.abs("1"); //Should be a number
"hello world".charAt("pos"); //Should be a number

//Also node stdlib
const fs = require('fs');
fs.rename('old'); //Too few arguments
