// @flow
//#### Unions ####//

// Union types allow you to combine types.
type cantDecide = boolean | number;
let alpha : cantDecide = 1;
alpha = false;
// alpha = "hello"; // This is a type error, string isn't there

//ASSIGNMENT: Specify the type of MyUnion
type MyUnion = any; //Change this
let union : MyUnion = 1;
union = 'hello';
union = { hello: 'world' };

// ASSIGNMENT: Change the function declaration to NOT include the ? character
function process(alpha: ?string, beta?: number ){
  if(alpha !== null && alpha !== undefined){
    alpha.charAt(1);
  }
  if(beta !== undefined){
    Math.abs(beta);
  }
}

// Union types really shine when used with objects.
type Cat = { type: 'cat', paws: number, name: string };
type Dog = { type: 'dog', paws: number, name: string, owner: string };
type Cow = { type: 'cow', legs: number, name: string };
type Animal = Cat | Dog | Cow;
function getsAnimal(animal: Animal){};
getsAnimal({ type: 'cat', paws: 4, name: 'Tigger' });

//ASSIGNMENT: Play around with literals in type definitions. 
