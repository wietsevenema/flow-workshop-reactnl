// @flow
//#### Sealing ####//

// Most of times, flow will warn you about dynamically adding properties
let person = { name: 'Peter' };
// person.age = 30; This wil warn: property age not found.
// This is called sealing. There are some exceptions to this:

// 1. Adding to an empty object literal
const newObject = {}
newObject.age = 30; // No error, not sealed
// 2. Object with spread properties
const copy = {...person};
copy.age = 30; // No error, not sealed
// 3. Object is a map
let map: { [key:string]: number } = {'one': 1, 'two': 2};
map['three'] = 3;

//ASSIGNMENT: Indicate with a comment if Sealed or Unsealed
const one = {'one': 80};
const two = {};
const three: {name?: string} = {};
const four = {'age': 30,...three};
const five: {} = {...{}};

// ### Exact object types ### //

// When you construct a sealed object, you can still supply
// too much parameters:
type Product = { name: string };
const product: Product = {
  name: 'Cardboard cutout James Kyle',
  price: 60,
};

// When we want to guarantee no added properties, use
// exact object types: use {| and |} instead of { and }.
// Where {x: string} contains at least the property x,
// {| x: string |} contains ONLY the property x

// ASSIGNMENT: Make sure poor little Tigger cannot have additional properties
type Pet = { name: string, creator: string };
const tigger : Pet = {
  name: 'Tigger',
  creator: 'Christopher Robin',
  honey: false
};
