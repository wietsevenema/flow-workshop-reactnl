// @flow
//#### Arrays and Tuples ####//

//Tuples vs. arrays
let arrayA: string[]; //Or Array<string>
arrayA = ['hello', 'world'];

let myTuple: [number, boolean];
myTuple = [1, true];

//ASSIGNMENT: Add types to B and C
let B = ['hello', 'world', 'and', null]; // Can you make this an array instead of tuple?
let C = [null, 'hello', true];

// Tuples can grow beyond bounds
let D = [true, 'hello'];

//QUESTION: What is the type of d3?
const d3 = D[42];

//ASSIGNMENT: Fix errors in types
let data: [number, number, string] = [1,2,true];
let myArray: [number] = [1, 's', 't', 3]; // Array, not tuple

//Arrays assumed dense and infinite
let E : string[] = ['h', 'e', 'l', 'l', 'o'];

//QUESTION: What is the type of fds?
const fds = E[99];

//QUESTION: Why is the type of fds not 'void | string'?
