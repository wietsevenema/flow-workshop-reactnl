// @flow
//#### Objects ####//

type Person = { name: string }; // This is how you provide a type alias
const peter: Person = { name: 'Peter' };

// ASSIGNMENT: Create one type definition for both books below.
// Use optional properties.

// type Book = ...

let book: Book = {
  title: 'Pride and Prejudice',
  author: 'Jane Austen',
  pages: 279,
  ISBN: '0679783261',
};

let anotherClassic: Book = {
  title: 'The Great Gatsby',
  author: 'F. Scott Fitzgerald',
  pages: 279,
  ASIN: 'B000FC0PDA',
};

//Objects a a map
const lookup = {
  '036': 'Almere',
  '030': 'Utrecht',
  '010': 'Rotterdam',
  '020': 'Amsterdam'
};
// You can type this with the bracket notation
(lookup: { [key:string]: string });

// ASSIGNMENT: Type ratings as a map:
const ratings = {
  'AAA': ['Netherlands', 'Luxembourg'],
  'AA+': ['Austria'],
  'AA':  ['Belgium', 'France'],
  'A':   ['Slovenia'],
  'BBB': ['Iceland', 'Italy']
};
