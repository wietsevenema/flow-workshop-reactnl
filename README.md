# Flow Workshop @ ReactNL

**Duration**: 2 hour workshop  
**Goal**: Get set up with flow and learn the most important concepts you need to be more productive with flow.  

**Prepare**:
 - Make sure new node (>6) is on your machine
 - Clone this repo.
 - Install Atom: https://atom.io
 - Add Nuclide to Atom: https://nuclide.io/docs/quick-start/getting-started/
 - Atom Settings: Packages > Nuclide > Settings
    - Check "Install Recommended Packages on Startup"
    - Scroll to Nuclide-flow, Check "Use the Flow binary included in each project's flow-bin"
 - Restart atom
 - cd exercises; npm install;

**General project setup**
 - Editor support (Atom with Nuclide)
 - Using flowtyped defs
 - How to ship code (babel)

**Adding types**
 - Intro
 - Simple types (any)
 - Mapping an object
   - Learn exact object types, sealed vs. unsealed
 - Arrays and tuples
   - Discover union types
 - Functions
   - Optional and maybe, first hint to refinements
 - Union types, literals
   - **FIXME**: Finish, add exercises.  
 - Classes / generics (nominal vs structured)
   - **FIXME**: Finish, add exercises.  
 - Refinements (mixed and any)
   - **FIXME**: Teach refinements (function met mixed input, typed output)
 - Modules, importing exporting types
   - **FIXME**: Not sure, should I cover this?
 - Typing React components
   - **FIXME**: Not sure, should I cover this?
