
/*
 1. Add flow and make sure flow checks this file
 2. Install flow-typed definitions
 3. There should now be a flow error in this file, fix it.
*/

const chalk = require('chalk');

if(chalk.supportColor === true){
  console.log(chalk.underline.bgBlue.red.bold('Hello world!'));
}
